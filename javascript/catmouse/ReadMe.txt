
The plots show an 8 component wave function in position and momentum space.  

Evolve button moves the wave functions forward in time by one time step.

Start and Stop buttons evolve the wave function forward in time automatically by ongoing steps.

Clicking on the wave function corresponds to making a measurement of the state vector eigen state corresponding to that component.  The result of measurement is calculated according to the normalized probability distribution over all possible states and the wave functin is collapsed accordign to the outcome of the mesasurement.

Periodic Start and Periodic Stop provide automatic measurement routines designed to explore the result of periodically measuring certain eigenstates.


