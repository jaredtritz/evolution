public class Atom {

	public double charge;
	public double mass;
	public int x;
	public int y;
	public double xDir;
	public double yDir;
	public boolean locked;
	public Molecule molecule;

	public Atom(int x, int y, double c, double m){
		this.x = x;
		this.y = y;
		charge = c;
		xDir = 0;
		yDir = 0;
		mass = m;
		reset();
	}

	public void reset(){
		if(locked){
			xDir = xDir*Evolution.momentum;
			yDir = yDir*Evolution.momentum;
		}else{
			xDir = 0;
			yDir = 0;
		}
		locked = false;
		molecule = null;
	}

}