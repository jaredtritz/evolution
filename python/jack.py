from random import randint
import numpy as np
deck = []
possible = [2,3,4,5,6,7,8,9,10,10,10,10,11]
for ii in range(1,10000000):
#for ii in range(1,1000):
    deck.append(possible[randint(0,12)])

class Player:

    def __init__(self, name, stay):
        self.name = name
        self.stay = stay
        self.hand = []
        self.past = []
        pass

    def add_card(self, card):
        self.hand.append(card)

    def stash(self):
        self.past.append(self.hand[:])
        self.hand = []

    def hit(self):
        if np.sum(self.hand) < self.stay:
            return True
        else:
            return False


smart = Player('smart', 12)  
foolish = Player('foolish', 12)  
dealer = Player('dealer', 17)  

while len(deck) > 100: 
    while(smart.hit()):
        card = deck.pop()
        smart.add_card(card)
    smart.stash() 

    while(dealer.hit()):
        card = deck.pop()
        dealer.add_card(card)
    dealer.stash() 

    while(foolish.hit()):
        card = deck.pop()
        foolish.add_card(card)
    foolish.stash() 

def tally(dealer, players):
    for pp in players: 
        win = 0
        loss = 0
        tot = 0
        for ii in range(0, len(pp.past)):
            if np.sum(pp.past[ii]) > 21:
                loss += 1
            elif np.sum(dealer.past[ii]) > 21:
                win += 1
            elif np.sum(pp.past[ii]) > np.sum(dealer.past[ii]):
                win += 1 
            else:
                loss += 1
            tot += sum(pp.past[ii])
        print "\n----" + str(pp.name) + "-----"
        print "stay: " + str(pp.stay)
        print "hands: " + str(len(pp.past))
        print "average: " + str(round(float(tot) / float(len(pp.past)),2))
        print "win ratio: " + str(round(float(win) / float(win + loss), 5))

tally(dealer=dealer, players=[smart, foolish]) 
