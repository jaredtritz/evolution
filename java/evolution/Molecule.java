
import java.util.*;

public class Molecule {

	public int id;
	public Vector atoms;
	public double xDir;
	public double yDir;

	public Molecule(int i){
		id = i;
		atoms = new Vector();
		xDir = 0;
		yDir = 0;
	}

	public void computePotential(){
		Atom a;
		xDir = 0; //Math.random()*atoms.size();
		yDir = 0; //Math.random()*atoms.size();
		for(int i=0;i<atoms.size();i++){
			a = (Atom)atoms.elementAt(i);
			xDir += a.xDir;
			yDir += a.yDir;
		}
	}

}