This is a log of simulation parameters and observations...


Change number: 9ed63de9b8e9bddc022a54d10d26d67ee53525a5

    > java Evolution 40 100 .1 .2 .1 5 2 100 -1
        Appears to form small molecules

    > java Evolution 40 100 .1 .2 .1 5 2 100 1
        Apears to form large molecules

    > java Evolution 40 100 .1 .2 .1 19 2 100 -1 
        Increasing the distance parameter larger molecules will form

    > java Evolution 40 100 .1 .2 .1 1 2 100 1
        Reducing the distance parameter molecules become much more stable

    > java Evolution 40 100 .1 .2 .1 5 2 100 -1 
    > java Evolution 40 100 .1 .2 .1 10 2 100 -1 
    > java Evolution 40 100 .1 .2 .1 20 2 100 -1 
    > java Evolution 40 100 .1 .2 .1 40 2 100 -1 
        The size of the structure formed is on the order of the distance over which interaction can occcur

    > java Evolution 40 100 .1 .2 .1 20 2 100 1
    > java Evolution 40 100 .1 .2 .1 20 2 100 -1
        On all distance scales the domain structures appear larger when like charges attract


Change number: ad83d9c1285a652592fc6d107538f376800a65b0

The distance scaling parameter doesn't appear to have much effect, but it would be neat to play two worlds side by side with different parameters to try and detect subtle differences.  In the real world forces drop off with distance squared because surface area of a sphere grows with distance squared and the effect of the force is distributed over the surface of a sphere with each new shell of volume.  By playing with this parameter you should be able to see how a force which is droppign off in four dimensions while only being experienced in two would behave.  

----------------------------------------

Shared this with Dinh and he mentioned:

http://forum.processing.org/topic/my-cellular-automata

I didn't run it b/c it seemed you have to download the code and I'm being quite lazy about it at the moment...maybe I was frusterated that javasript of all things wasn't already prepared to run when viewing it in a web browser.Or maybe I just missed the link to run it?

He also shared this link: 

http://www.shiffman.net/teaching/nature/week7/

---------------------------

Testing ssh key so I don't have to type password in every time I commit... 
