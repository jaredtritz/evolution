

var Vbar =
{
    // graphs
    m_height : 128,
    m_width : 360,
    m_bars : 8,
    m_barwidth : 0, //15*6,
    m_border : 2,
    m_canvas1 : null,
    m_domain1 : null,
    m_caption1 : null,
    
    m_canvas2 : null,
    m_domain2 : null,
    m_caption2 : null,
            
    construct : function ()
    {
        this.m_barwidth = this.m_width / this.m_bars;
        
        // graph
        this.m_canvas1 = document.getElementById('bargraph1');
        this.m_canvas2 = document.getElementById('bargraph2');

        if (this.m_canvas1.getContext)
        {
            this.m_domain1 = this.m_canvas1.getContext('2d');
            this.m_domain2 = this.m_canvas2.getContext('2d');
        } 
        else 
        {
            alert('canvas object not supported!');
            // canvas-unsupported code here
        }               
    },
               
    paint : function ()
    { // paint stats by screen
    
        //this.erase();
    
        this.m_caption1.innerHTML = this.model.m_caption1; 
        this.m_caption1.innerHTML = this.model.m_caption2; 
    },     
             
    erase : function()
    {
        // drawing code here
        this.m_domain1.fillStyle = CONST.COLOR_WHITE;
        this.m_domain1.fillRect(0,0,360, 128);     
        
        this.m_domain2.fillStyle = CONST.COLOR_WHITE;
        this.m_domain2.fillRect(0,0,360, 128);         
        /*
        this.m_domain.fillStyle    = '#00f';
        this.m_domain.font         = 'italic 30px sans-serif';
        this.m_domain.textBaseline = 'top';
        this.m_domain.fillText  ('Calculating...', 100, 50);    
        */
    },

    erasebar1 : function (number)
    {
        // vars
        var ULx = (number - 1) * this.m_barwidth;
        var ULy = 0;
        var width = this.m_barwidth;
        var dipto = this.m_height; 

        this.m_domain1.fillStyle = CONST.COLOR_WHITE;
        this.m_domain1.fillRect(ULx, ULy, width, dipto);           
    },
         
    drawbar1 : function (number, height, color)
    {      
        // vars
        var ULx = ((number - 1) * this.m_barwidth) + 1;
        var ULy = this.m_height - height;
        var width = this.m_barwidth - 1;
        var dipto = this.m_height;     
        
        // unpaint
        this.erasebar1(number);
        
        // paint
        this.m_domain1.fillStyle = color;
        this.m_domain1.fillRect(ULx, ULy, width, dipto);             
    },
    
    erasebar2 : function (number)
    {
        // vars
        var ULx = (number - 1) * this.m_barwidth;
        var ULy = 0;
        var width = this.m_barwidth;
        var dipto = this.m_height; 

        this.m_domain2.fillStyle = CONST.COLOR_WHITE;
        this.m_domain2.fillRect(ULx, ULy, width, dipto);           
    },
         
    drawbar2 : function (number, height, color)
    {      
        // vars
        var ULx = ((number - 1) * this.m_barwidth) + 1;
        var ULy = this.m_height - height;
        var width = this.m_barwidth - 1;
        var dipto = this.m_height;     
        
        // unpaint
        this.erasebar2(number);
        
        // paint
        this.m_domain2.fillStyle = color;
        this.m_domain2.fillRect(ULx, ULy, width, dipto);             
    },
    
    findposition1 : function(x, y)
    { // pass in global coords, get the relative stuff local here
        var obj = this.m_canvas1;
        var curleft = curtop = 0;
        var newx;
        var newy;

        // find object offsets
        if (obj.offsetParent)
        {
            do 
            {
                curleft += obj.offsetLeft;
                curtop += obj.offsetTop;            
            }
            while (obj = obj.offsetParent);
        }

        // calc relative from object offsets
        newx = x - curleft - this.m_border;
        newy = y - curtop - this.m_border;
    
        // range validation
        if(newx > this.m_width - 1)
            newx = this.m_width - 1;
        if(newx < 0)
            newx = 0;
        if(newy > this.m_height - 1)
            newy = this.m_height - 1;
        if(newy < 0)
            newy = 0;

        // flip axis
        newy = this.m_height - newy;

        //return new Array(newx, newy);   
        
        return (Math.ceil(newx / this.m_barwidth));
        
    }  
}