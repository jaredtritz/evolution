import java.util.*;

public class World {

	public double posThresh = .1;
	public double negThresh = .2;

	public Atom[][] atoms;
	public Vector molecules;

	public World(int rows, int columns, double posThresh, double negThresh){
		this.posThresh = posThresh;
		this.negThresh = negThresh;
		atoms = new Atom[rows][columns];
		int c;
		double r;
		for(int i=0;i<atoms.length;i++){
			for(int j=0;j<atoms[i].length;j++){
				r = Math.random();
				c = 0;
				if(r < posThresh){
					c = 1;
				}else if(r < negThresh){
					c = -1;
				}
				atoms[i][j] = new Atom(j, i, c, 1);
			}
		}
		molecules = new Vector();
	}

	public void markMolecules(){
		molecules = new Vector();
		int id = 1;
		for(int y=0;y<atoms.length;y++){
			for(int x=0;x<atoms[y].length;x++){
				if(atoms[y][x].charge!=0 && atoms[y][x].molecule==null){
					Molecule m = new Molecule(id);
					id++;
					molecules.add(m);
					markMolecule(x, y, atoms[y][x].charge, m);
				}
			}
		}
	}

	public void markMolecule(int x, int y, double charge, Molecule m){
		if(atoms[y][x].molecule==null && atoms[y][x].charge==charge){

			Atom a;

			atoms[y][x].molecule = m;
			m.atoms.add(atoms[y][x]);

			a = getNeighbor(x, y, 0, 1);
			markMolecule(a.x, a.y, charge, m);

			a = getNeighbor(x, y, 0, -1);
			markMolecule(a.x, a.y, charge, m);

			a = getNeighbor(x, y, 1, 0);
			markMolecule(a.x, a.y, charge, m);

			a = getNeighbor(x, y, -1, 0);
			markMolecule(a.x, a.y, charge, m);

		}
	}

	public boolean move(){
		boolean moved = false;
		double r = Math.random();
		if(r < .25){
			for(int y=0;y<atoms.length;y++){
				for(int x=0;x<atoms[y].length;x++){
					if(move(x, y)){
						moved = true;
					}
				}
			}
		}else if(r < .5){
			for(int y=0;y<atoms.length;y++){
				for(int x=atoms[y].length-1;x>=0;x--){
					if(move(x, y)){
						moved = true;
					}
				}
			}
		}else if(r < .75){
			for(int y=atoms.length-1;y>=0;y--){
				for(int x=0;x<atoms[y].length;x++){
					if(move(x, y)){
						moved = true;
					}
				}
			}
		}else{
			for(int y=atoms.length-1;y>=0;y--){
				for(int x=atoms[y].length-1;x>=0;x--){
					if(move(x, y)){
						moved = true;
					}
				}
			}
		}
		return moved;
	}

	public boolean move(int x, int y){
		Atom a = atoms[y][x];
		Atom n;
		if(!a.locked){

			int xDir, yDir;

			//  && a.charge!=n.charge don't allow atoms with the same charge to switch with each other
			if(Math.abs(a.xDir) > Math.abs(a.yDir)){ // stronger desire to move horizontally

				xDir = a.xDir > 0 ? 1 : -1;
				yDir = 0;
				n = getNeighbor(x, y, xDir, yDir);

				if(!n.locked && n.xDir * a.xDir <= 0 && n.xDir >= n.yDir){
					n.locked = true;
					a.locked = true;
					atoms[y][x] = atoms[n.y][n.x];
					atoms[n.y][n.x] = a;
					a.x = n.x;
					a.y = n.y;
					n.x = x;
					n.y = y;
					//System.out.println("Swapped (" + a.y + ", " + a.x + ") with (" + n.y + ", " + n.x + ")");
					return true;
				}

			}else if(Math.abs(a.xDir) < Math.abs(a.yDir)){ // stronger desire to move vertically

				xDir = 0;
				yDir = a.yDir > 0 ? 1 : -1;
				n = getNeighbor(x, y, xDir, yDir);

				if(!n.locked && n.yDir * a.yDir <= 0 && n.yDir >= n.xDir){
					n.locked = true;
					a.locked = true;
					atoms[y][x] = atoms[n.y][n.x];
					atoms[n.y][n.x] = a;
					a.x = n.x;
					a.y = n.y;
					n.x = x;
					n.y = y;
					//System.out.println("Swapped (" + a.y + ", " + a.x + ") with (" + n.y + ", " + n.x + ")");
					return true;
				}

			}else if(a.yDir!=0){ // equal desire to move horizontal and vertical

				if(Math.random() < .5){

					// try horizontal first
					xDir = a.xDir > 0 ? 1 : -1;
					yDir = 0;
					n = getNeighbor(x, y, xDir, yDir);

					if(!n.locked && n.xDir * a.xDir <= 0 && n.xDir >= n.yDir){

						n.locked = true;
						a.locked = true;
						atoms[y][x] = atoms[n.y][n.x];
						atoms[n.y][n.x] = a;
						a.x = n.x;
						a.y = n.y;
						n.x = x;
						n.y = y;
						//System.out.println("Swapped (" + a.y + ", " + a.x + ") with (" + n.y + ", " + n.x + ")");
						return true;

					}

					xDir = 0;
					yDir = a.yDir > 0 ? 1 : -1;
					n = getNeighbor(x, y, xDir, yDir);

					if(!n.locked && n.yDir * a.yDir <= 0 && n.yDir >= n.xDir){
						n.locked = true;
						a.locked = true;
						atoms[y][x] = atoms[n.y][n.x];
						atoms[n.y][n.x] = a;
						a.x = n.x;
						a.y = n.y;
						n.x = x;
						n.y = y;
						//System.out.println("Swapped (" + a.y + ", " + a.x + ") with (" + n.y + ", " + n.x + ")");
						return true;
					}

				}else{

					xDir = 0;
					yDir = a.yDir > 0 ? 1 : -1;
					n = getNeighbor(x, y, xDir, yDir);

					if(!n.locked && n.yDir * a.yDir <= 0 && n.yDir >= n.xDir){
						n.locked = true;
						a.locked = true;
						atoms[y][x] = atoms[n.y][n.x];
						atoms[n.y][n.x] = a;
						a.x = n.x;
						a.y = n.y;
						n.x = x;
						n.y = y;
						//System.out.println("Swapped (" + a.y + ", " + a.x + ") with (" + n.y + ", " + n.x + ")");
						return true;
					}

					// try horizontal first
					xDir = a.xDir > 0 ? 1 : -1;
					yDir = 0;
					n = getNeighbor(x, y, xDir, yDir);

					if(!n.locked && n.xDir * a.xDir <= 0 && n.xDir >= n.yDir){

						n.locked = true;
						a.locked = true;
						atoms[y][x] = atoms[n.y][n.x];
						atoms[n.y][n.x] = a;
						a.x = n.x;
						a.y = n.y;
						n.x = x;
						n.y = y;
						//System.out.println("Swapped (" + a.y + ", " + a.x + ") with (" + n.y + ", " + n.x + ")");
						return true;

					}

				}

			}
		}
		return false;
	}

	public void computeTotalPotential(){
		computePotential();
		//printPotentials();
		markMolecules();
		Molecule m;
		for(int i=0;i<molecules.size();i++){
			m = (Molecule)molecules.elementAt(i);
			m.computePotential();
		}
		//printMolecules();
		//printMoleculePotentials();
		addMoleculePotentials();
		//printPotentials();
	}

	public void addMoleculePotentials(){
		for(int y=0;y<atoms.length;y++){
			for(int x=0;x<atoms[y].length;x++){
				if(atoms[y][x].molecule != null){
					atoms[y][x].xDir += (Evolution.moleculeInfluence*atoms[y][x].molecule.xDir);
					atoms[y][x].yDir += (Evolution.moleculeInfluence*atoms[y][x].molecule.yDir);
				}
			}
		}
	}

	public void computePotential(){
		for(int y=0;y<atoms.length;y++){
			for(int x=0;x<atoms[y].length;x++){
				computePotential(x, y);
			}
		}
	}

	public void computePotential(int x, int y){
		Atom a = atoms[y][x];
		a.reset();
		if(a.charge!=0){
			Atom n;
			double dist;
			int delta = Evolution.neighborhoodSize;
			for(int i=-delta;i<=delta;i++){
				for(int j=-delta;j<=delta;j++){
					if(i!=0 || j!=0){
						n = getNeighbor(x, y, j, i);
						if(n.charge!=0){
							dist = Math.sqrt(i*i + j*j);
                            double drop_off = 1/(Math.pow(dist, Evolution.force_scaling));
							if(n.charge==a.charge){
								a.yDir += Evolution.attraction*i*(1/drop_off);
								a.xDir += Evolution.attraction*j*(1/drop_off);
							}else{
								a.yDir -= Evolution.attraction*i*(1/drop_off);
								a.xDir -= Evolution.attraction*j*(1/drop_off);
							}
						}
					}
				}
			}
		}
	}

	public Atom getNeighbor(int x, int y, int xDir, int yDir){

		int newY = (y+yDir);
		if(newY < 0){
			newY += atoms.length;
		}else if(newY >= atoms.length){
			newY -= atoms.length;
		}

		int newX = (x+xDir);
		if(newX < 0){
			newX += atoms[0].length;
		}else if(newX >= atoms[0].length){
			newX -= atoms[0].length;
		}

		return atoms[newY][newX];

	}

	public void printMoleculePotentials(){
		System.out.println();
		for(int i=0;i<atoms.length;i++){
			for(int j=0;j<atoms[i].length;j++){
				if(atoms[i][j].molecule==null){
					System.out.print("(-.-,-.-)\t");
				}else{
					System.out.print("(" + atoms[i][j].molecule.xDir + "," + atoms[i][j].molecule.yDir + ")\t");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	public void printPotentials(){
		System.out.println();
		for(int i=0;i<atoms.length;i++){
			for(int j=0;j<atoms[i].length;j++){
				System.out.print("(" + atoms[i][j].xDir + "," + atoms[i][j].yDir + ")\t");
			}
			System.out.println();
		}
		System.out.println();
	}

	public void printMolecules(){
		System.out.println();
		for(int i=0;i<atoms.length;i++){
			for(int j=0;j<atoms[i].length;j++){
				if(atoms[i][j].molecule==null){
					System.out.print("-\t");
				}else{
					System.out.print(atoms[i][j].molecule.id + "\t");
				}
			}
			System.out.println();
		}
		System.out.println();
	}

	public String toString(){
		return toString(1);
	}

	public String toString(int dim){
		StringBuffer result = new StringBuffer();
		for(int i=0;i<atoms.length;i++){
			for(int j=0;j<atoms[i].length;j++){
				if(atoms[i][j].charge==-1){
					result.append("-");
				}else if(atoms[i][j].charge==1){
					result.append("#");
				}else{
					result.append(" ");
				}
			}
			if(dim > 1){
				result.append("\n");
			}
		}
		return result.toString();
	}

}
