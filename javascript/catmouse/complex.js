
// constructor
function Complex(real, imaginary) {
    this.x = real;       // The real part of the number
    this.y = imaginary;  // The imaginary part of the number
}

// INSTANCE METHODS

// Return the magnitude of a complex number.
Complex.prototype.magnitude = function() {
    return (this.x*this.x + this.y*this.y);
};

// Return the probability amplitude of a complex number.
Complex.prototype.prob = function() {
    return (this.x*this.x + this.y*this.y);
};

// Display complex number
Complex.prototype.inspect = function() {
    //alert("{" + this.x + "," + this.y + "}");
    alert("{" + Math.round(this.x*100)/100 + "," + Math.round(this.y*100)/100 + "}");   // control decimals
};

// Return string representation of complex number
Complex.prototype.toString = function() {
    //return "{" + this.x + "," + this.y + "}";
    return "{" + Math.round(this.x*100)/100 + "," + Math.round(this.y*100)/100 + "}";   // control decimals
};

// Return the real portion of a complex number.
Complex.prototype.real = function() { return this.x; }

// CLASS METHODS

// Add two complex numbers and return the result.
Complex.add = function (a, b) {
    return new Complex(a.x + b.x, a.y + b.y);
};

// Subtract one complex number from another.
Complex.subtract = function (a, b) 
{
    return new Complex(a.x - b.x, a.y - b.y);
};

// Multiply two complex numbers and return the product.
Complex.multiply = function(a, b) {
    return new Complex(a.x * b.x - a.y * b.y,
                       a.x * b.y + a.y * b.x);
};

Complex.divide = function(a, b)
{
    var denom, x, y;
    
    denom = (b.x * b.x) + (b.y * b.y);
    x = ((a.x * b.x) + (a.y * b.y)) / denom;
    y = ((a.y * b.x) - (a.x * b.y)) / denom;
    
    return new Complex(x,y);
};

Complex.raise = function(z, n)
{
    var ii, res;
    
    if(n == 0)
        return Complex.create(1,0);
    
    res = Complex.create(z.x, z.y);
    
    for(ii=1; ii<n; ii++)
    {
        res = Complex.multiply(z, res); 
    }
    
    return res;
};

// Conjugate a complex number
Complex.conjugate = function(z)
{
    return new Complex(z.x, -z.y);
};

// Constructor function
Complex.create = function(x, y) 
{
    if(isNaN(x))
        x = 0;
        
    if(isNaN(y))
        y = 0;

  return new Complex(x, y);
};

Complex.createExp = function(r, a)
{
    var x, y;
    
    if(isNaN(r))
        r = 0;
        
    if(isNaN(a))
        a = 0;
        
    x = r * Math.cos(a);
    y = r * Math.sin(a);
        
    return new Complex(x, y);
};

// Here are some useful predefined complex numbers.
// They are defined as class properties, where they can be used as
// "constants." (Note, though, that they are not actually read-only.)
Complex.zero = new Complex(0,0);
Complex.one = new Complex(1,0);
Complex.i = new Complex(0,1);