import math

class Opening:

    def __init__(self, normal):
        self.normal = normal

class House:
 
    def __init__(self, north, opening):
        self.north = north # angle in radians from north pole
        self.opening = opening # [east,west] in cartesian coordinates

class Earth:
    tilt = 0.42 # radian tilt ~ 24.3 degrees
    # average temperature maps etc...
     
    def __init__(self):
        pass

class Sim:
    # in this simulation:
    # 0) spherical coordinates are used with origin at center of earth
    # 1) the sun is located at [r, pi/2, 0] and never moves 
    # 2) the earth center of mass never moves
    # 3) the earth rotation axis is tilted toward the sun to start -- summer solstice
    # 4) the earth tilt axis is rotated about the z axis [r, 0, theta] to simulate time of year
    # 5) day records are made by spinning the earth about it's tilted axis 360 degrees
    # 6) day records are histories of the dot product between sun direction and opening normal

    sun_pos = [1, math.pi/2, 0] 

    def __init__(self, earth, house):
        self.earth = earth
        self.house = house
        # symetry time parts for simplicity, i.e. (day_parts : year_parts = 1)
        self.time_parts = 52    # corresponds to ~1 week/year and ~30 minutes/day
        # spacial position
        self.pos_phi = self.house.north + self.earth.tilt
        self.pos_theta = 0
        # simulation rotation axis
        self.year_axis = [1,0,0]                # [r,phi,theta] in spherical coords
        self.day_axis = [1,self.earth.tilt,0]   # [r,phi,theta] in spherical coords

    def make_record(self):
        self.history = []
        for yy in range(0,self.time_parts):
            self.pos_year(yy)                       # position the year axis
            self.history.append(self.spin_day())    # make a day record

    def pos_year(self, frac_year):
        # assume simulation begins at the summer solstice
        # rotate year axis by fraction of year 
        
        pass

    def spin_day(self):
        # spin day axis 360 degrees
        day = []
        for dd in range(0,self.time_parts):
            # make a record of opening normal dotted with sun position vector
            #dot_prod = ??? 
            #day.append()
            pass
        return day

    def record(self):
        self.history.append([self.pos_phi, self.pos_theta])

    def report(self):
        print self.pos_phi
        print self.pos_theta
        print self.time_parts
        print self.history

earth = Earth()
house = House(north=(math.pi/4), opening=Opening([1,1,1]))
sim = Sim(earth, house)

sim.make_record()
sim.report()

#from visual import *
#redbox=box(pos=vector(4,2,3), size=(8,4,6),color=color.red)
#ball=sphere(pos=vector(4,7,3),radius=2,color=color.green)

import numpy as np

def rotation_matrix(axis,theta):
    axis = axis/np.sqrt(np.dot(axis,axis))
    a = np.cos(theta/2)
    b,c,d = -axis*np.sin(theta/2)
    return np.array([[a*a+b*b-c*c-d*d, 2*(b*c-a*d), 2*(b*d+a*c)],
                     [2*(b*c+a*d), a*a+c*c-b*b-d*d, 2*(c*d-a*b)],
                     [2*(b*d-a*c), 2*(c*d+a*b), a*a+d*d-b*b-c*c]])

v = np.array([0,1,1])
axis = np.array([0,0,1])
theta = math.pi/2

print(np.dot(rotation_matrix(axis,theta),v))   
