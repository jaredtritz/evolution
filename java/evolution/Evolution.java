

public class Evolution {

	public static double momentum;
	public static int neighborhoodSize;
	public static double moleculeInfluence;
	public static double attraction;
	public static double force_scaling;

	public static void main(String[] args){

		if(args==null || args.length < 10){
			System.err.println("java Evolution <rows> <cols> <posThresh> <negThresh> <momentum> <neighborhood> <moleculeInfluence> <sleepMillis> <attraction> <forceScaling>");
			return;
		}

		momentum = Double.parseDouble(args[4]);
		neighborhoodSize = Integer.parseInt(args[5]);
		moleculeInfluence = Double.parseDouble(args[6]);
		int sleepMillis = Integer.parseInt(args[7]);
		attraction = Double.parseDouble(args[8]);
		force_scaling = Double.parseDouble(args[9]);

		String[] prevWorlds = new String[1];
		int prevWorldIndex = 0;

		World w = new World(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));

		w.computeTotalPotential();

		prevWorlds[prevWorldIndex] = w.toString(2);
		System.out.println("Initial\n" + prevWorlds[prevWorldIndex]);
		prevWorldIndex = (prevWorldIndex+1) % prevWorlds.length;

		int it = 1;
		boolean stop = false;

		while(!stop && w.move()){

			w.computeTotalPotential();

			prevWorlds[prevWorldIndex] = w.toString(2);
			System.out.println("Iteration " + (it++) + "\n" + prevWorlds[prevWorldIndex]);

			/*
			for(int i=0;i<prevWorlds.length;i++){
				if(i!=prevWorldIndex){
					if(prevWorlds[i]!=null && prevWorlds[i].equals(prevWorlds[prevWorldIndex])){
						stop = true;
					}
				}
			}
			*/

			prevWorldIndex = (prevWorldIndex+1) % prevWorlds.length;

			try{ Thread.sleep(sleepMillis); }catch(Exception e){}

		}

		System.out.println("Final\n" + w.toString(2));

	}

}



